var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);
const axios = require("axios");
var path = require("path");

const PlayerMoves = require("./predict.js");

const shortid = require("shortid");

const baseUrl = "http://imt1x.com/ludo/api/";

var numUsers = 0;

var roomData = new Map();
var gameRooms = [];
var clientRoom = new Map();
var userIdRoom = new Map();
var userStore = new Map();
var socketUserId = new Map();

var gameStartTimeout = new Map();
var gameTurnTimer = new Map();
var botTurnTimer = new Map();

testing = async () => {
  console.log("test");
  try {
    console.log("await start");
    const res = await axios.post(
      baseUrl + "/declarePrivateWinner",
      `user_id=${11}&tour_id=${"test"}`
    );
    console.log("declare result", res.data);
    console.log("await end");
  } catch (error) {
    console.error(error);
  }
};

io.on("connection", socket => {
  console.log("new user connected", socket.id);
  numUsers++;

  socket.emit("linked");

  socket.on("checkIfRoomAvail", userData => {
    console.log("checkIfRoomAvail called");

    let roomId = userIdRoom[userData.userId];

    if (roomId != null) {
      //already connected make reconnect procedure

      socket.emit("room_avail", {
        status: true,
        message: "active room avail"
      });
    } else {
      // create or join some random room

      socket.emit("room_avail", {
        status: false,
        message: "active room not available"
      });
    }
  });

  // when the client emits 'add user', this listens and executes
  socket.on("joinOrCreateRoom", userData => {
    console.log("join or create room called");

    userStore[userData.userId] = userData;

    socketUserId[socket.id] = userData.userId;

    let roomId = userIdRoom[userData.userId];

    if (roomId != null) {
      //already connected make reconnect procedure

      reconnect(socket, userData, roomId);
    } else {
      // create or join some random room
      if (gameRooms.length > 0) {
        checkRoomAvail(socket, userData);
      } else {
        createRoom(socket, userData);
      }
    }
  });

  socket.on("pawn_move", userData => {
    // console.log("pawn moved called : ", userData);

    let roomId = userIdRoom[userData.userId];

    let currentPlayerIndex = roomData[roomId].currentPlayerIndex;
    let currentPlayerId = roomData[roomId].userIds[currentPlayerIndex];

    let currentPlayerData = roomData[roomId].players[currentPlayerId];

    if (
      checkPawnWhichCanBeMoved(
        roomId,
        currentPlayerId,
        currentPlayerData.lastDiceCount
      ).indexOf(userData.extra - 1) < 0
    ) {
      // console.log("pawn cannot moved");
      return;
    }

    if (currentPlayerData.userId != userData.userId) {
      console.log("Invalid access");
      return;
    }

    if (currentPlayerData.pawnLocked) {
      // console.log("pawn is already lock");
      return;
    }

    var diceValue = userData.value;

    currentPlayerData.diceLocked = true;
    currentPlayerData.pawnLocked = true;

    roomData[roomId].players[currentPlayerId] = currentPlayerData;

    var diceValue = currentPlayerData.lastDiceCount;

    sendRoomUpdate(roomId, "pawn_moved", "pawn has been played", "success", {
      currentPlayerIndex: roomData[roomId].currentPlayerIndex,
      pawnId: userData.extra
    });
    updatePawnPosition(roomId, currentPlayerId, userData.extra, diceValue);
  });

  socket.on("dice_turn", userData => {
    // console.log("dice turn called : ", userData);

    let roomId = userIdRoom[userData.userId];

    let currentPlayerIndex = roomData[roomId].currentPlayerIndex;
    let currentPlayerId = roomData[roomId].userIds[currentPlayerIndex];

    let currentPlayerData = roomData[roomId].players[currentPlayerId];

    if (currentPlayerData.userId != userData.userId) {
      console.log("Invalid access");
      return;
    }

    if (currentPlayerData.diceLocked) {
      // console.log("dice is already lock");
      return;
    }
    setupTurnTimer(roomId);

    currentPlayerData.diceLocked = true;
    currentPlayerData.pawnLocked = false;
    currentPlayerData.diceRolling = true;

    diceValue = getDiceValue(roomId, currentPlayerId);
    //diceValue = userData.value;

    currentPlayerData.lastDiceCount = diceValue;

    roomData[roomId].players[currentPlayerId] = currentPlayerData;

    sendRoomUpdate(roomId, "dice_turn", "dice turned", "success", {
      currentPlayerIndex: roomData[roomId].currentPlayerIndex
    });

    let currentClientId = currentPlayerId;
    setTimeout(() => {
      roomData[roomId].players[currentPlayerId].diceRolling = false;

      sendRoomUpdate(roomId, "dice_stopped", "dice stopped", "success", {
        currentPlayerIndex: roomData[roomId].currentPlayerIndex
      });

      let movablePawn = checkPawnWhichCanBeMoved(
        roomId,
        currentClientId,
        diceValue
      );

      if (movablePawn.length == 0) {
        roomData[roomId].players[currentPlayerId].pawnLocked = true;

        changeTurn(roomId);
      }
    }, 1);
  });

  // when the user disconnects.. perform this
  socket.on("disconnect", () => {
    console.log("user disconnected ", socketUserId[socket.id]);

    // console.log("user disconnected ", socket.id);

    // userStore[userData.userId] = userData;

    // socketUserId[socket.id] = userData.userId;

    //  let roomId =  userIdRoom[userData.userId];

    var roomId = clientRoom[socket.id];

    if (roomId == null) {
      console.log("no room assinged");
      return;
    }
    if (roomData[roomId] != null) {
      //roomData[roomId].playerCount--;
    }

    let userId = socketUserId[socket.id];

    if (userId != null && roomId != null) {
      //roomData[roomId].players[userId].connected = false;
    }

    clientRoom.delete(socket.id);

    // echo globally that this client has left

    // io.in(roomId).emit('room_update', {
    //   event: "room_left",
    //   message: "user left the room",
    //   type:"success",
    //   data:roomData[roomId],
    //   roomId: roomId,
    //   extras:{
    //     userId: userStore[socketUserId[socket.id]].userId,
    //     userName:  userStore[socketUserId[socket.id]].name,
    //     position:roomData[roomId].userIds.indexOf(userStore[socketUserId[socket.id]].userId)
    //   }
    // });
  });
});

const createRoom = (socket, userData) => {
  console.log("create room");

  let roomId = shortid.generate();

  gameRooms.push(roomId);
  userIdRoom[userData.userId] = roomId;

  let players = new Map();

  players[userData.userId] = {
    userId: userData.userId,
    connected: true,
    playerUniversalIndex: 1,
    playerType: 1,
    name: userData.name,
    score: 0,
    pawns: [1, 1, -1, -1],
    diceLocked: true,
    pawnLocked: true,
    diceRolling: false,
    lastDiceCount: 1
  };


  let pathArray = [];


  roomData[roomId] = {
    roomId: roomId,
    ownerId: userData.userId,
    userIds: [`${userData.userId}`],
    playerCount: 1,
    canAddPlayer: true,
    isRoundComplete: false,

    moveTypes: ["move", "kill", "open", "home", "skip", "six", "safe"],

    usernames: ["km", "bot1", "bot2", "bot3", "skip", "six", "safe"],

    userScores: [0, 0, 0, 0],

    userMoves: [],
    userMovesTypes: [0, 0, 0, 0],
    userSecondChanceCount: [0, 0, 0, 0],
    userThirdChanceCount: [0, 0, 0, 0],
    userKillOrHomeChanceCount: [0, 0, 0, 0],

    pathStates: pathArray,
    pathPlayerStartIndex: [27, 40, 1, 14],
    pathSafeZones: [9, 22, 35, 48, 27, 40, 1, 14],

    pathBlueBonusZonesIndex: [23, 36, 49, 10],
    pathBlackBonusZonesIndex: [24, 37, 50, 11],
    pathPinkBonusZonesIndex: [25, 38, 51, 12],

    mapUniversalIndexToSessionId: [],
    isBot: [false],

    isTournament: false,
    tourId: 0,
    currentPlayerIndex: 0,
    currentRound: 1,
    isGameStart: false,
    isGameEnded: false,
    players: players,
    currentIndex: 1
  };

  addSingleBot(roomId);

  //assinging data to user moves
  new PlayerMoves(value => {
    roomData[roomId].userMoves[0] = value;
  });

  socket.join(roomId);
  clientRoom[socket.id] = roomId;

  let position = roomData[roomId].userIds.indexOf(userData.userId);

  let playerData = roomData[roomId].players[userData.userId];

  io.to(roomId).emit("entity_add", {
    event: "entity_add",
    message: "room has new player",
    type: "success",
    data: roomData[roomId],
    roomId: roomId,
    extras: {
      userId: userData.userId,
      userName: userData.name,
      position: position
    },
    playerData: playerData
  });

  io.to(roomId).emit("room_init", {
    event: "room_created",
    message: "new room has been created",
    type: "success",
    data: roomData[roomId],
    roomId: roomId,
    extras: {
      userId: userData.userId,
      userName: userData.name,
      position: position
    }
  });

  sendRoomUpdate(roomId, "room_init", "room initialized", "success", {
    userId: userData.userId,
    userName: userData.name,
    position: position
  });

  io.in(roomId).emit("room_update", {
    event: "room_created",
    message: "new room has been created",
    type: "success",
    data: roomData[roomId],
    roomId: roomId,
    extras: {
      userId: userData.userId,
      userName: userData.name,
      position: position
    },
    playerData: playerData
  });

  gameStartTimeout[roomId] = setTimeout(() => {
    console.log(`Adding Bots to game`);

    addBotToGame(roomId);
  }, 45000); // wait for bots default 45000 ms
};

const addSingleBot = roomId => {
  console.log("single bot added called");

  let roomState = roomData[roomId];

  roomState.playerCount++;

  //assinging data to user moves

  let playerCount = roomState.playerCount;
  let botId = shortid.generate();

  roomState.userIds.push(botId);

  roomState.players[botId] = {
    userId: botId,
    connected: false,
    playerUniversalIndex: roomState.playerCount,
    playerType: 0,
    name: botId,
    score: 0,
    pawns: [-1, -1, -1, -1],
    diceLocked: true,
    pawnLocked: true,
    diceRolling: false,
    lastDiceCount: 0
  };

  roomState.isBot[roomState.players[botId].playerUniversalIndex - 1] = true;

  roomData[roomId] = roomState;

  new PlayerMoves(value => {
    roomData[roomId].userMoves[playerCount] = value;
  });

  sendRoomUpdate(roomId, "bot_added", "bot has been added to game", "success", {
    botId: botId
  });
};

const addBotToGame = roomId => {
  roomData[roomId].canAddPlayer = false;

  let roomState = roomData[roomId];

  const botCount = 4 - roomState.playerCount;
  console.log(`Adding Bots to game : ${botCount}`);

  for (let i = 0; i < botCount; i++) {
    roomState.playerCount++;

    //assinging data to user moves

    let playerCount = roomState.playerCount;
    let botId = shortid.generate();

    roomState.userIds.push(`${botId}`);

    roomState.players[botId] = {
      userId: botId,
      connected: true,
      playerUniversalIndex: roomState.playerCount,
      playerType: roomState.playerCount % 2 == 0 ? 0 : 1,
      name: botId,
      score: 0,
      pawns: [1, 1, -1, -1],
      diceLocked: true,
      pawnLocked: true,
      diceRolling: false,
      lastDiceCount: 1
    };

    roomState.isBot[roomState.players[botId].playerUniversalIndex - 1] = true;

    roomData[roomId] = roomState;

    new PlayerMoves(value => {
      roomData[roomId].userMoves[playerCount] = value;
    });

    let position = roomData[roomId].userIds.indexOf(botId);
    let botData = roomData[roomId].players[botId];

    io.to(roomId).emit("entity_add", {
      event: "entity_add",
      message: "room has new player",
      type: "success",
      data: roomData[roomId],
      roomId: roomId,
      extras: {
        userId: botId,
        userName: botId,
        position: position
      },
      playerData: botData
    });

    sendRoomUpdate(
      roomId,
      "bot_added",
      "bot has been added to game",
      "success",
      {
        botId: botId
      }
    );

    io.in(roomId).emit("room_update", {
      event: "room_created",
      message: "bot initialised",
      type: "success",
      data: roomData[roomId],
      roomId: roomId,
      extras: {
        botId: botId
      },
      playerData: botData
    });
  }

  startGame(roomId);
};

const startGame = roomId => {
  setTimeout(() => {
    roomData[roomId].isGameStart = true;

    console.log("game started : ", roomData[roomId]);
    setupTurnTimer(roomId);

    let currentUserSessionId =
      roomData[roomId].userIds[roomData[roomId].currentPlayerIndex];

    //unlock the dice for user
    roomData[roomId].players[currentUserSessionId].diceLocked = false;

    sendRoomUpdate(roomId, "start_game", "game started", "success", {
      currentPlayerIndex: roomData[roomId].currentPlayerIndex
    });

    if (roomData[roomId].isBot[roomData[roomId].currentPlayerIndex]) {
      //Play Bot Turn
      playBotTurn(roomId);
      // console.log("Bot Turn started");
    } else {
      // console.log("Player turn started");
    }
  }, 5000); // start game after 5000 seconds default
};

const setupTurnTimer = roomId => {
  let turnTimer = gameTurnTimer[roomId];

  if (turnTimer != null && turnTimer != undefined) clearInterval(turnTimer);
  gameTurnTimer[roomId] = setInterval(() => {
    // console.log("Timer turn called");

    //if (this.state.isGameStart) this.changeTurn();
    if (roomData[roomId] != null) {
      if (roomData[roomId].isGameStart) playAutoMove(roomId);
    }
  }, 15000); // default turn timer is 15000 ms
};

const playAutoMove = roomId => {
  try {
    // console.log("playing auto move");

    let sessionId =
      roomData[roomId].userIds[roomData[roomId].currentPlayerIndex];

    // console.log('session id for play auto move : ', sessionId);

    let diceValue = 2;
    if (roomData[roomId].players[sessionId].diceLocked) {
      // console.log("dice is already lock");
      diceValue = roomData[roomId].players[sessionId].lastDiceCount;
    } else {
      diceValue = getDiceValue(roomId, sessionId);
    }

    setupTurnTimer(roomId);

    roomData[roomId].players[sessionId].diceLocked = true;
    roomData[roomId].players[sessionId].pawnLocked = false;
    roomData[roomId].players[sessionId].diceRolling = true;

    roomData[roomId].players[sessionId].lastDiceCount = diceValue;

    roomData[roomId].players[sessionId].diceLocked = true;
    roomData[roomId].players[sessionId].pawnLocked = true;

    let movablePawn = checkPawnWhichCanBeMoved(roomId, sessionId, diceValue);

    if (movablePawn.length > 0) {
      let randPawnId = movablePawn[0];
      // console.log("pawn to move : " + randPawnId);

      setTimeout(() => {
        roomData[roomId].players[sessionId].diceRolling = false;

        updatePawnPosition(
          roomId,
          sessionId,
          randPawnId + 1,
          diceValue,
          roomId
        );
      }, 1);
    } else {
      // console.log("no movable pawn, change auto turn");
      roomData[roomId].players[sessionId].diceRolling = false;

      changeTurn(roomId);
    }

    sendRoomUpdate(roomId, "play_auto_move", "playing auto move", "success", {
      currentPlayerIndex: roomData[roomId].currentPlayerIndex,
      diceValue: diceValue
    });
  } catch (e) {
    console.log("game has been destroyed probably : " + roomId);
  }
};

const getDiceValue = (roomId, sessionId) => {
  let currentPlayer = roomData[roomId].currentPlayerIndex;
  let currentRound = roomData[roomId].currentRound;

  switch (roomData[roomId].userMovesTypes[currentPlayer]) {
    case 0:
      // normal move

      let diceValueOne =
        roomData[roomId].userMoves[currentPlayer].dices[currentRound];
      if (diceValueOne == 6) roomData[roomId].userMovesTypes[currentPlayer] = 1;
      return diceValueOne;

      break;
    case 1:
      // after the six

      let secondChanceValue =
        roomData[roomId].userSecondChanceCount[currentPlayer];
      let diceValueTwo =
        roomData[roomId].userMoves[currentPlayer].secondDice[secondChanceValue];

      roomData[roomId].userMovesTypes[currentPlayer] = 0;
      if (diceValueTwo == 6) roomData[roomId].userMovesTypes[currentPlayer] = 2;

      roomData[roomId].userSecondChanceCount[currentPlayer]++;

      return diceValueTwo;

      break;
    case 2:
      // after the double six

      let thirdChanceValue =
        roomData[roomId].userThirdChanceCount[currentPlayer];

      roomData[roomId].userMovesTypes[currentPlayer] = 0;
      roomData[roomId].userThirdChanceCount[currentPlayer]++;
      return roomData[roomId].userMoves[currentPlayer].thirdDice[
        thirdChanceValue
      ];
      break;

    case 3:
      // after kill move

      let killOptions = [4, 5, 4, 3, 5, 3];
      roomData[roomId].userMovesTypes[currentPlayer] = 0;

      let currentKillRound =
        roomData[roomId].userKillOrHomeChanceCount[currentPlayer];

      if (currentKillRound == 5) {
        roomData[roomId].userKillOrHomeChanceCount[currentPlayer] = 0;
      } else {
        roomData[roomId].userKillOrHomeChanceCount[currentPlayer]++;
      }

      return killOptions[currentKillRound];

      break;

    default:
      return roomData[roomId].userMoves[currentPlayer].dices[currentRound];

      break;
  }
};

const checkPawnWhichCanBeMoved = (roomId, sessionId, diceValue) => {
  let canMovePawn = [];
  roomData[roomId].players[sessionId].pawns.map((pawnPosition, index) => {
    if (diceValue == 6) {
      if (pawnPosition + 6 < 58) canMovePawn.push(index);
    } else {
      if (pawnPosition == -1) {
      } else {
        if (pawnPosition + diceValue < 58) canMovePawn.push(index);
      }
    }
    return true;
  });
  // console.log(canMovePawn);

  return canMovePawn;
};

const updatePawnPosition = (roomId, sessionId, pawnId, diceValue) => {
  // roomData[roomId].players[sessionId]
  let currentPlayerIndex = roomData[roomId].currentPlayerIndex;
  let currentPlayerServerIndex = roomData[roomId].userIds[currentPlayerIndex];
  let currentRound = roomData[roomId].currentRound;

  roomData[roomId].players[sessionId].score += diceValue;
  roomData[roomId].userScores[currentPlayerIndex] += diceValue;

  let pathPawnPlayer;
  let isKilled = false;
  var pawns = roomData[roomId].players[sessionId].pawns;

  var pawnLastPosition = pawns[pawnId - 1];
  //  console.log( `pawn ${pawnId} last pos ${pawnLastPosition}`);

  let updatePathPosition;
  if (pawnLastPosition === -1) {
    if (diceValue == 6) {
      updatePathPosition = 1;
      roomData[roomId].players[sessionId].pawns[
        pawnId - 1
      ] = updatePathPosition;
      roomData[roomId].players[sessionId].score -= diceValue;
      roomData[roomId].userScores[currentPlayerIndex] -= diceValue;
    }
  } else {
    let tempUpdatePathPosition = pawnLastPosition + diceValue;

    if (tempUpdatePathPosition <= 57) {
      updatePathPosition = pawnLastPosition + diceValue;

      roomData[roomId].players[sessionId].pawns[
        pawnId - 1
      ] = updatePathPosition;

      if (updatePathPosition == 57) {
        //TODO pawn is in fort! disabled it.
      }
    } else {
      updatePathPosition = pawnLastPosition;
    }
  }

  let lastPathModifiedIndex =
    roomData[roomId].pathPlayerStartIndex[currentPlayerIndex] +
    (pawnLastPosition - 1);

  if (lastPathModifiedIndex >= 52) lastPathModifiedIndex -= 52;
  if (pawnLastPosition >= 0) {
    if (roomData[roomId].pathStates[lastPathModifiedIndex]) {
      var index = roomData[roomId].pathStates[lastPathModifiedIndex].indexOf(
        `${currentPlayerIndex}${pawnId}`
      );
      if (index > -1) {
        //let pathPawnData = this.pathStates[this.pathPlayerStartIndex[this.state.currentPlayerIndex-1] + (pawnLastPosition-1)];

        roomData[roomId].pathStates[lastPathModifiedIndex].splice(index, 1);
      }
    }
  }

  if (updatePathPosition) {
    let currentPathModifiedIndex =
      roomData[roomId].pathPlayerStartIndex[currentPlayerIndex] +
      (updatePathPosition - 1);
    if (currentPathModifiedIndex >= 52) currentPathModifiedIndex -= 52;

    if (updatePathPosition >= 52) {
      let overallPathCount =
        roomData[roomId].pathPlayerStartIndex[currentPlayerIndex] +
        (updatePathPosition - 1);
      // if pawn is inside the safe path
      if (roomData[roomId].pathStates[overallPathCount]) {
        roomData[roomId].pathStates[overallPathCount].push(
          `${currentPlayerIndex}${pawnId}`
        );
      } else {
        roomData[roomId].pathStates[overallPathCount] = [
          `${currentPlayerIndex}${pawnId}`
        ];
      }
    } else {
      // if pawn is in the normal paths
      if (roomData[roomId].pathStates[currentPathModifiedIndex]) {
        let currentPathPawnsArray =
          roomData[roomId].pathStates[currentPathModifiedIndex];
        //console.log(currentPathPawnsArray);
        if (
          roomData[roomId].pathSafeZones.indexOf(currentPathModifiedIndex) <= -1
        ) {
          currentPathPawnsArray.map(pathPawnId => {
            pathPawnPlayer = pathPawnId[0];
            // console.log(pathPawnPlayer);
            let pathPawnIndex = pathPawnId[1];
            if (pathPawnPlayer != currentPlayerIndex) {
              let pawnCountOnSamePath = currentPathPawnsArray.filter(
                item => item[0] === pathPawnPlayer
              );
              // console.log(pawnCountOnSamePath);
              if (pawnCountOnSamePath) {
                let pawnCount = pawnCountOnSamePath.length;
                // console.log("pawn count :" + pawnCount);

                if (pawnCount % 2 != 0) {
                  var index = roomData[roomId].pathStates[
                    currentPathModifiedIndex
                  ].indexOf(pathPawnId);
                  //console.log(index);
                  if (index > -1) {
                    //console.log(`pawn killed ${pathPawnPlayer}${pathPawnIndex} by the pawn ${this.state.currentPlayerIndex - 1}${pawnId} at position ${currentPathModifiedIndex}`);
                    roomData[roomId].pathStates[
                      currentPathModifiedIndex
                    ].splice(index, 1);
                    isKilled = true;

                    roomData[roomId].players[
                      roomData[roomId].userIds[pathPawnPlayer]
                    ].pawns[pathPawnIndex - 1] = -1;

                    //console.log("Player or bot  got extra turn because of kill");
                    roomData[roomId].userMovesTypes[currentPlayerIndex] = 3;

                    // check if player is on blue  bonus zones
                    if (
                      roomData[roomId].pathBlueBonusZonesIndex.indexOf(
                        currentPathModifiedIndex
                      ) >= 0
                    ) {
                      // if(this.pathBlueBonusZonesIndex.indexOf(currentPathModifiedIndex) == (this.state.currentPlayerIndex-1)){
                      roomData[roomId].players[sessionId].score += 30;
                      roomData[roomId].userScores[currentPlayerIndex] += 30;

                      //}
                    }
                    // check if player is on blue  bonus zones
                    else if (
                      roomData[roomId].pathBlackBonusZonesIndex.indexOf(
                        currentPathModifiedIndex
                      ) >= 0
                    ) {
                      // if(this.pathBlackBonusZonesIndex.indexOf(currentPathModifiedIndex) == (this.state.currentPlayerIndex-1)){

                      roomData[roomId].players[sessionId].score += 35;
                      roomData[roomId].userScores[currentPlayerIndex] += 35;

                      // }
                    }

                    // check if player is on blue  bonus zones
                    else if (
                      roomData[roomId].pathPinkBonusZonesIndex.indexOf(
                        currentPathModifiedIndex
                      ) >= 0
                    ) {
                      //if(this.pathPinkBonusZonesIndex.indexOf(currentPathModifiedIndex) == (this.state.currentPlayerIndex-1)){

                      roomData[roomId].players[sessionId].score += 40;
                      roomData[roomId].userScores[currentPlayerIndex] += 40;

                      //}
                    } else {
                      roomData[roomId].players[sessionId].score += 20;
                      roomData[roomId].userScores[currentPlayerIndex] += 20;
                    }

                    if (
                      roomData[roomId].players[
                        roomData[roomId].userIds[pathPawnPlayer]
                      ].score >= 5
                    ) {
                      roomData[roomId].players[
                        roomData[roomId].userIds[pathPawnPlayer]
                      ].score -= 5;
                      roomData[roomId].userScores[pathPawnPlayer] -= 5;
                    }
                  }
                } else {
                }
              } else {
              }
            }
          });
        } else {
          // console.log("No kill safe zone "+currentPathModifiedIndex);
        }
        roomData[roomId].pathStates[currentPathModifiedIndex].push(
          `${currentPlayerIndex}${pawnId}`
        );
      } else {
        roomData[roomId].pathStates[currentPathModifiedIndex] = [
          `${currentPlayerIndex}${pawnId}`
        ];
      }
    }

    sendRoomUpdate(roomId, "pawn_moved", "pawn moved", "success", {
      currentPlayerIndex: roomData[roomId].currentPlayerIndex
    });

    if (isKilled) {
      let currentPlayerIndex = roomData[roomId].currentPlayerIndex;
      let killedPlayerData =
        roomData[roomId].players[roomData[roomId].userIds[pathPawnPlayer]];

      if (!pathPawnPlayer) return;
      io.in(roomId).emit("room_update", {
        event: "pawn_killed",
        message: "pawn killed",
        type: "success",
        data: roomData[roomId],
        roomId: roomId,
        extras: {
          currentPlayerIndex: currentPlayerIndex
        },
        playerData: killedPlayerData
      });
    }

    //console.log(JSON.stringify(this.pathStates));

    // if (!this.isBot[this.state.playerUniversalIndex - 1]) this.setupTurnTimer();

    if (updatePathPosition != 57) {
      if (diceValue == 6 || isKilled) {
        // console.log("Player or bot  got extra turn because of 6");

        if (diceValue == 6) {
          giveExtraTurn(roomId, sessionId, "Dice is 6");
        } else {
          giveExtraTurn(roomId, sessionId, "Killing ");
        }
      } else {
        changeTurn(roomId);
      }
    } else {
      if (updatePathPosition == pawnLastPosition) {
        // console.log("already in fort");
      } else {
        // console.log("pawn reached fort.");
        roomData[roomId].players[sessionId].score += 20;
        roomData[roomId].userMovesTypes[currentPlayerIndex] = 3;
        roomData[roomId].userScores[currentPlayerIndex] += 20;

        giveExtraTurn(roomId, sessionId, "Pawn reached fort");
      }
      //TODO pawn is in home! cannot be played
    }
  } else {
    changeTurn(roomId);

    // console.log("pawn is in home!");
  }

  // if(this.isTournament){
  //   if(this.isBot[currentPlayerIndex])return;
  //   this.updateTournamentScore({
  //     score: this.userScores[currentPlayerIndex],
  //     round : currentRound,
  //     userId: currentPlayerServerIndex
  //   });
  // }
};

const giveExtraTurn = (roomId, sessionId, message) => {
  // console.log({
  //   gameId: this.tourId,
  //   isTournament:this.isTournament,
  //   currentTurn:this.state.currentPlayerIndex,
  //   currentRound: this.state.currentRound,
  //   userId:     this.state.entities[this.mapUniversalIndexToSessionId[this.state.currentPlayerIndex-1]].userId,
  //   message:"Extra turn given : "+message
  // });

  // this.state.timerValue = this.timerTurnSwitch.currentTime();

  //unlock the dice for user
  roomData[roomId].players[sessionId].diceLocked = false;

  sendRoomUpdate(roomId, "extra_turn", "extra turn given", "success", {
    currentPlayerIndex: roomData[roomId].currentPlayerIndex
  });

  setupTurnTimer(roomId);

  if (roomData[roomId].isBot[roomData[roomId].currentPlayerIndex]) {
    //Play Bot Turn
    playBotTurn(roomId);
  } else {
  }

  // console.log("Extra turn Given");
};

const changeTurn = roomId => {
  // this.state.timerValue = this.timerTurnSwitch.currentTime()();
  //this.timerTurnSwitch.reset();

  //this.state.entities[this.mapUniversalIndexToSessionId[this.state.currentPlayerIndex-1]].bonusPoint = 0;

  roomData[roomId].currentIndex++;
  roomData[roomId].currentIndex++;

  if (roomData[roomId].currentIndex > 4) {
    roomData[roomId].currentPlayerIndex = 0;
    roomData[roomId].currentIndex = 1;
  } else {
    roomData[roomId].currentPlayerIndex = roomData[roomId].currentIndex - 1;
  }

  if (roomData[roomId].currentPlayerIndex == 0) {
    roomData[roomId].currentRound += 1;
  }

  roomData[roomId].players[
    roomData[roomId].userIds[roomData[roomId].currentPlayerIndex]
  ].diceLocked = false;

  setupTurnTimer(roomId);
  if (
    roomData[roomId].currentRound == 48 &&
    roomData[roomId].currentPlayerIndex == 0
  ) {
    roomData[roomId].isGameEnded = true;

    let turnTimer = gameTurnTimer[roomId];

    if (turnTimer != null && turnTimer != undefined) clearInterval(turnTimer);

    if (botTurnTimer[roomId] != undefined) clearInterval(botTurnTimer[roomId]);

    setTimeout(async () => {
      console.log("Need to dispose room : ", roomData[roomId]);

      // Setup the winner logic

      //  userIds:[`${userData.userId}`],

      //  userScores : [0,0,0,0],
      //   isBot : [false],

      let userScores = [...roomData[roomId].userScores];

      let winnerOrder = userScores.sort().reverse();

      let winnerScore = winnerOrder[0];

      let indexOfWinner = roomData[roomId].userScores.indexOf(winnerScore);

      let isWinnerRealPlayer = !roomData[roomId].isBot[indexOfWinner];

      console.log(
        roomData[roomId].userScores,
        userScores,
        winnerOrder,
        winnerScore,
        indexOfWinner,
        isWinnerRealPlayer
      );
      if (isWinnerRealPlayer) {
        let winnerUserId = roomData[roomId].userIds[indexOfWinner];

        console.log("real winner ", winnerUserId);

        try {
          const res = await axios.post(
            baseUrl + "/declarePrivateWinner",
            `user_id=${winnerUserId}&tour_id=${roomId}`
          );

          console.log("declare result", res.data);
        } catch (error) {
          console.error(error);
        }
      } else {
        console.log("bot winner");
      }

      roomData[roomId].userIds.forEach(userId => {
        console.log("user id : ", userId);
        userIdRoom.delete(userId);

        userIdRoom[userId] = null;
      });

      console.log("remove users : ", userIdRoom);

      sendRoomUpdate(
        roomId,
        "game_finished",
        "player turn has been changed",
        "success",
        {
          currentPlayerIndex: roomData[roomId].currentPlayerIndex
        }
      );

      setTimeout(() => {
        io.in(roomId).clients((error, clients) => {
          console.log("clients in the room: \n");
          console.log(clients);
          clients.forEach(socket_id => {
            io.sockets.sockets[socket_id].leave(roomId);
            socketUserId[socket_id] = null;
          });
        });

        console.log(turnTimer);

        // roomData[roomId] = null;
      }, 2000);
    }, 2000);

    return;
  }

  sendRoomUpdate(
    roomId,
    "turn_change",
    "player turn has been changed",
    "success",
    {
      currentPlayerIndex: roomData[roomId].currentPlayerIndex
    }
  );

  if (roomData[roomId].isBot[roomData[roomId].currentPlayerIndex]) {
    //Play Bot Turn
    playBotTurn(roomId);
  } else {
  }
};

const playBotTurn = roomId => {
  try {
    botTurnTimer[roomId] = setInterval(() => {
      var diceValue = getDiceValue(roomId, "");

      // diceValue = 2;
      //var diceValue = 6;

      let currentClientId =
        roomData[roomId].userIds[roomData[roomId].currentPlayerIndex];
      roomData[roomId].players[currentClientId].lastDiceCount = diceValue;
      roomData[roomId].players[currentClientId].diceRolling = true;

      sendRoomUpdate(roomId, "bot_play", "bot turn played start", "success", {
        currentPlayerIndex: roomData[roomId].currentPlayerIndex
      });

      setTimeout(() => {
        roomData[roomId].players[currentClientId].diceRolling = false;
        roomData[roomId].players[currentClientId].diceLocked = true;
        roomData[roomId].players[currentClientId].pawnLocked = false;

        sendRoomUpdate(
          roomId,
          "bot_play",
          "bot turn played stared",
          "success",
          {
            currentPlayerIndex: roomData[roomId].currentPlayerIndex
          }
        );

        console.log("check which pawn can be moved", roomData[roomId].currentPlayerIndex);
        let movablePawn = checkPawnWhichCanBeMoved(
          roomId,
          currentClientId,
          diceValue
        );
        // var randPawnId = Math.floor(Math.random() * (movablePawn.length))  ;

        if (movablePawn.length > 0) {
          var randPawnId = whichPawnToMove(
            roomId,
            currentClientId,
            movablePawn,
            diceValue
          );

          randPawnId++;

          setTimeout(() => {
            roomData[roomId].players[currentClientId].pawnLocked = true;

            updatePawnPosition(roomId, currentClientId, randPawnId, diceValue);
          }, 1000);
        } else {
          changeTurn(roomId);
        }
      }, 1);

      if (botTurnTimer[roomId] != undefined)
        clearInterval(botTurnTimer[roomId]);
    }, 2500);
  } catch (e) {
    console.log("game has been destroyed : " + roomId);
  }
};

const whichPawnToMove = (roomId, sessionId, pawnsToMove, diceValue) => {
  let pawnToMove;

  let pawnAvailToKill = checkIfPawnCanKill(
    roomId,
    sessionId,
    pawnsToMove,
    diceValue
  );


  // console.log("pawn available to kill",pawnAvailToKill);

  if (pawnAvailToKill !== undefined) {
    if (pawnAvailToKill.length > 0) pawnToMove = pawnAvailToKill[0];
  }

  //console.log("panw to kill ");
  //console.log(pawnAvailToKill);

  if (pawnToMove !== undefined) {
    return pawnToMove;
  } else {
    if (diceValue == 6) {
      let myPaws = roomData[roomId].players[sessionId].pawns;

      let openablePawns = myPaws.map((pawn, index) => {
        if (pawn == -1) return index;
      });

      openablePawns = openablePawns.filter(pawn => pawn != undefined);

      // console.log("openable pawns");
      //console.log(openablePawns);

      if (openablePawns !== undefined) {
        if (openablePawns.length > 0) pawnToMove = openablePawns[0];
      }
    }
  }
  if (pawnToMove !== undefined) {
    return pawnToMove;
  } else {
    return (pawnToMove = pawnsToMove[0]);
  }
};

const checkIfPawnCanKill = (roomId, sessionId, pawnsToMove, diceValue) => {



  // console.log("check for kill" , {
  //   pawntoMove: pawnsToMove,
  //   diceValue: diceValue,
  //   pathState : roomData[roomId].pathStates,
  //   pawns : roomData[roomId].players[sessionId].pawns,
  //   sessionId: sessionId,
  //   opponentPawn : roomData[roomId].players['11'].pawns,
  //   currentPlayerIndex: roomData[roomId].currentPlayerIndex

  // })
  let isKilled = false;

  let whichPawnCanKill = [];
  var pawns = roomData[roomId].players[sessionId].pawns;

  let pawnAvailToKill = pawnsToMove.filter(pawnId => {
    var pawnLastPosition = pawns[pawnId];
    let updatePathPosition;

    if (pawnLastPosition !== -1) {
      let tempUpdatePathPosition = pawnLastPosition + diceValue;

      if (tempUpdatePathPosition < 57) {
        updatePathPosition = pawnLastPosition + diceValue;

        if (updatePathPosition) {
          let currentPathModifiedIndex =
            roomData[roomId].pathPlayerStartIndex[
              roomData[roomId].currentPlayerIndex
            ] +
            (updatePathPosition - 1);
          if (currentPathModifiedIndex >= 52) currentPathModifiedIndex -= 52;

          if (updatePathPosition < 52) {
            if (roomData[roomId].pathStates[currentPathModifiedIndex]) {
              let currentPathPawnsArray =
                roomData[roomId].pathStates[currentPathModifiedIndex];
              //  console.log(currentPathPawnsArray);
              if (
                roomData[roomId].pathSafeZones.indexOf(
                  currentPathModifiedIndex
                ) <= -1
              ) {
                currentPathPawnsArray.map(pathPawnId => {
                  let pathPawnPlayer = pathPawnId[0];
                  // console.log("path pawnn player" , pathPawnPlayer);
                  let pathPawnIndex = pathPawnId[1];
                  if (pathPawnPlayer != roomData[roomId].currentPlayerIndex) {
                    let pawnCountOnSamePath = currentPathPawnsArray.filter(
                      item => item[0] === pathPawnPlayer
                    );
                    // console.log("count on same path",pawnCountOnSamePath);
                    if (pawnCountOnSamePath) {
                      let pawnCount = pawnCountOnSamePath.length;
                      //console.log("pawn count :" + pawnCount);

                      if (pawnCount % 2 != 0) {
                        var index = roomData[roomId].pathStates[
                          currentPathModifiedIndex
                        ].indexOf(pathPawnId);
                        //  console.log("index to kill in pathstate",index);
                        if (index > -1) {
                          isKilled = true;
                          // console.log("kill true");

                          whichPawnCanKill.push(pawnId);
                          return pawnId;
                        }
                      }
                    }
                  }
                });
              }
            }
          }
        }
      }
    }
  });
  console.log(whichPawnCanKill);
  return whichPawnCanKill;
};

const checkRoomAvail = (socket, userData) => {
  let isJoined = false;
  let roomId;
  let position = 0;

  // check if any previous room available or not
  gameRooms.forEach(room => {
    if (isJoined) return;
    let roomState = roomData[room];

    if (!roomState) return;

    if (roomState.playerCount < 3) {
      console.log("join room");

      let roomState = roomData[room];
      position = roomState.playerCount + 1;
      roomId = room;
      userIdRoom[userData.userId] = roomId;

      roomState.playerCount++;

      roomState.userIds.push(`${userData.userId}`);

      roomData[room] = roomState;

      new PlayerMoves(value => {
        roomData[roomId].userMoves[position - 1] = value;
      });

      roomData[roomId].players[userData.userId] = {
        userId: userData.userId,
        connected: true,
        playerUniversalIndex: roomState.playerCount,
        playerType: 1,
        name: userData.name,
        score: 0,
        pawns: [1, 1, -1, -1],
        diceLocked: true,
        pawnLocked: true,
        diceRolling: false,
        lastDiceCount: 1
      };

      roomState.isBot[
        roomState.players[userData.userId].playerUniversalIndex - 1
      ] = false;

      isJoined = true;

      socket.join(roomId);
      clientRoom[socket.id] = roomId;

      //addSingleBot(roomId);
    }
  });

  // if user not joined to any previous room create a new room!
  if (!isJoined) createRoom(socket, userData);
  else {
    // io.to(roomId).emit("entity_add",{
    //   event: "entity_add",
    //   message: "room has new player",
    //   type:"success",
    //   data:roomData[roomId],
    //   roomId: roomId,
    //  extras: {
    //   userId: userData.userId,
    //   userName: userData.name,
    //   position:roomData[roomId].userIds.indexOf(userData.userId),
    //  },
    //  playerData: roomData[roomId].players[userData.userId]

    // });

    roomData[roomId].userIds.forEach(id => {
      let playerData = roomData[roomId].players[id];
      let position = roomData[roomId].userIds.indexOf(id);

      io.to(roomId).emit("entity_add", {
        event: "entity_add",
        message: "room has new player",
        type: "success",
        data: roomData[roomId],
        roomId: roomId,
        extras: {
          userId: id,
          userName: playerData.name,
          position: position
        },
        playerData: playerData
      });
    });

    let position = roomData[roomId].userIds.indexOf(userData.userId);

    io.to(roomId).emit("room_update", {
      event: "room_joined",
      message: " room has been joined",
      type: "success",
      data: roomData[roomId],
      roomId: roomId,
      extras: {
        userId: userData.userId,
        userName: userData.name,
        position: position
      }
    });

    let playerData = roomData[roomId].players[userData.userId];
    io.in(roomId).emit("room_update", {
      event: "room_created",
      message: "player added",
      type: "success",
      data: roomData[roomId],
      roomId: roomId,
      extras: {
        userId: userData.userId,
        userName: userData.name,
        position: position
      },
      playerData: playerData
    });
  }
};

const reconnect = (socket, userData, roomId) => {
  console.log("reconnect room");

  let roomState = roomData[roomId];

  // console.log(roomState);

  socket.join(roomId);
  clientRoom[socket.id] = roomId;

  let position = roomData[roomId].userIds.indexOf(userData.userId);

  socket.emit("room_update", {
    event: "reconnect",
    message: "player reconnected properly",
    type: "success",
    data: roomData[roomId],
    roomId: roomId,
    extras: {
      userId: userData.userId,
      userName: userData.name,
      position: position
    }
  });

  roomData[roomId].userIds.forEach((id, index) => {
    let playerData = roomData[roomId].players[id];

    setTimeout(() => {
      let playerData = roomData[roomId].players[id];
      socket.emit("entity_add", {
        event: "entity_add",
        message: "room has new player",
        type: "success",
        data: roomData[roomId],
        roomId: roomId,
        extras: {
          userId: id,
          userName: playerData.name,
          position: index
        },
        playerData: playerData
      });
    }, 1);
  });

  roomData[roomId].userIds.forEach((id, index) => {
    setTimeout(() => {
      let playerData = roomData[roomId].players[id];

      socket.emit("entity_update", {
        event: "entity_update",
        message: "room has new player",
        type: "success",
        data: roomData[roomId],
        roomId: roomId,
        extras: {
          userId: id,
          userName: playerData.name,
          position: index
        },
        playerData: playerData
      });
    });
  }, 1);
};

const sendRoomUpdate = (roomId, event, message, type, extras) => {
  // console.log({
  //   event: event,
  //   message: message,
  //   type:type,
  //   data:roomData[roomId],
  //   roomId: roomId,
  //   extras:extras,
  //   pathState: roomData[roomId].pathStates
  // });

  let currentPlayerIndex = roomData[roomId].currentPlayerIndex;

  let currentPlayerId = roomData[roomId].userIds[currentPlayerIndex];
  let playerData = roomData[roomId].players[currentPlayerId];

  io.in(roomId).emit("room_update", {
    event: event,
    message: message,
    type: type,
    data: roomData[roomId],
    roomId: roomId,
    extras: extras,
    playerData: playerData
  });
};

const PORT = Number(process.env.PORT || 5000);

app.get("/", function(req, res) {
  // res.sendFile(path.join(__dirname + '/index.html'));
  res.json({ health: "green" });
});


app.get("/play", function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
  // res.json({ health: "green" });
});

http.listen(PORT, function() {
  console.log("listening on *: " + PORT);
});
