var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);
const axios = require("axios");

const PlayerMoves = require("./predict.js");

const shortid = require("shortid");

const baseUrl = "http://imt1x.com/ludo/api/";

var numUsers = 0;

var roomData = [];
var gameRooms = new Map();
var clientRoom = new Map();
var userIdRoom = new Map();
var userStore = new Map();
var socketUserId = new Map();

var gameStartTimeout = new Map();
var gameTurnTimer = new Map();
var botTurnTimer = new Map();


io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('disconnect', function(){
        console.log('user disconnected');
    });


    socket.on('player_ans', function(payload){
        console.log('player ans');



        io.to(`${payload.roomId}`).emit('player_ans',payload );

        if(payload.isBot){

            let options = payload.options.length;
            io.to(`${payload.roomId}`).emit('opponent_ans',{...payload, opAns: 0} );

        }

    });

    socket.on('opponent_ans', function(payload){
        console.log('opponent ans');

        io.to(`${payload.roomId}`).emit('opponent_ans',payload );

    });


    socket.on('next_que', function(payload){
        console.log('next que');

        io.to(`${payload.roomId}`).emit('next_que',payload );

    });

    socket.on('game_finish', function(payload){
        console.log('game finish');

        io.to(`${payload.roomId}`).emit('game_finish',payload );

    });


    socket.on('join_or_create', function(payload){
        console.log('join_or_create : payload: ',  payload);


        let openRooms  = [];
        roomData.forEach((value, key) => { 

            if(!value.isFilled && value.ownerId != payload.ownerId){

                delete roomData[key];
                openRooms.push(value)
            }
          });


        console.log("open_room" , openRooms);
        if(openRooms.length > 0){
            // Open room available
            let firstRoom = openRooms[0];
            let roomDataObj = Object.assign({}, firstRoom , {
                isFilled : true,
                updatedAt : new Date(),
                opponentId : payload.playerId

            });

            roomData.push(roomDataObj);

            socket.join(`${firstRoom.roomId}`);

           
            gameRooms[firstRoom.roomId] = roomDataObj;

            io.to(`${firstRoom.roomId}`).emit('opponent_joined',roomDataObj );
            io.to(`${firstRoom.roomId}`).emit('game_started',roomDataObj );



        }else{
            //create new room
           let  roomId = shortid.generate();

            let roomDataObj = {
                isFilled : false,
                createdAt : new Date(),
                roomId,
                ownerId: payload.playerId,
                quizId: payload.quizId,
                gameId: payload.gameId
            };

            let roomIndex = roomData.push(roomDataObj) - 1;

            gameRooms[roomId] = roomDataObj;
            socket.join(`${roomId}`);

            socket.emit('room_joined' , roomDataObj);


            setTimeout(async () =>  {

                // add random player
                const response = await axios.get('https://randomuser.me/api/?results=1&gender=female');

                while(response.data.error){
                     response = await axios.get('https://randomuser.me/api/?results=1&gender=female');

                }
                console.log("user",response.data.results[0], response.data.results[0]);

                let opponentDataApi = response.data.results[0];


                if(opponentDataApi.location.country){
                    
                  let  countryData = await axios.get(`https://restcountries.eu/rest/v2/name/${opponentDataApi.location.country}`);

                  if(countryData.data.length > 0){
                      opponentDataApi['countryFlag'] = countryData.data[0].flag;
                  }

                }

                let opponentData= {
                    first_name : `${opponentDataApi.name.first} ${opponentDataApi.name.last}`,
                    country: opponentDataApi.location.country,
                    age: opponentDataApi.dob.age > 38 ? opponentDataApi.dob.age - 10 : opponentDataApi.dob.age ,
                    imageUrl: opponentDataApi.picture.large,
                    flag: opponentDataApi.countryFlag
                };

                let roomInfo = gameRooms[roomId];
                if(!roomInfo.isFilled){

                    let roomDataObj = Object.assign({}, roomInfo , {
                        isFilled : true,
                        updatedAt : new Date(),
                        opponentId : shortid.generate(),
                        opponentData: opponentData,
                        isBot: true
        
                    });
                    delete roomData[roomIndex];
                    roomData.push(roomDataObj);
        
                    // socket.join(`${firstRoom.roomId}`);
        
                   
                    gameRooms[roomId] = roomDataObj;
        
                    io.to(`${roomId}`).emit('opponent_joined',roomDataObj );

                    setTimeout(()=>{
                        io.to(`${roomId}`).emit('game_started',roomDataObj );

                    }, 5000);
        
        

                }
            },3000)

        }
    });
    
});

const PORT = Number(process.env.PORT || 5000);

app.get("/", function(req, res) {
  // res.sendFile(path.join(__dirname + '/index.html'));
  res.json({ health: "green" });
});

http.listen(PORT, function() {
  console.log("listening on *: " + PORT);
});
