class PlayerMoves {

    constructor(callback) {
       this.getDices(callback);
    }
    


 getDices(callback){

    let isAvail = false;

    let getStartDice = setInterval(() => {

        if(isAvail)return;
        this.getDiceArray(function(value){
            // number generated
            if(value.diceSum == 160){
    
                clearTimeout(getStartDice); 
    
                 if(isAvail)return;
    
                isAvail =true;
                // console.log(JSON.stringify(value));
                callback(value);
                
    
            }
        });
    
    },1);
}





getDiceArray(callback){
let roundCount = 48;
let numberRepeatCount = [0,0,0,0,0,0];
let currentCount =0;

let maxSum = 160;
let currentSum = 0;



let maxSixCount = 8;
let diceValues = [];

let secondDiceValue = [];
let thirdDiceValue = [];
let isKillandHomeReset = 0;

let isSecondChance = false;
let isThirdChance = false;

let roundArray = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48];
let secondRoundValue = [4,6,1,2,4,6,5,5];
let thirdRoundValue = [1,5];

let arr1 = [2,6,4,1,4,2];
let arr2 = [2,6,4,3,2,5];
let arr3 = [2,6,4,5,2,3];
let arr4 = [3,6,3,2,2,3];
let arr5 = [3,6,3,4,1,1];
let arr6 = [6,5,1,5,3,1];
let arr7 = [5,6,5,1,5,3];
let arr8 = [1,6,1,1,2,3];


let orderArray = [0,1,2,3,4,5,6,7];

function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}

shuffle(orderArray);
shuffle(arr1);
shuffle(arr2);
shuffle(arr3);
shuffle(arr4);
shuffle(arr5);
shuffle(arr6);
shuffle(arr7);
shuffle(arr8);


let items = [];

items.push(arr1);
items.push(arr2);
items.push(arr3);
items.push(arr4);
items.push(arr5);
items.push(arr6);
items.push(arr7);
items.push(arr8);


let finalArray = [];

orderArray.map(item => {
    finalArray = finalArray.concat(items[item])
});

let secondRoundValueRandom = [];
let randomRoundArray = [];
// for (roundArray, i = roundArray.length; i--; ) {
//     var random = roundArray.splice(Math.floor(Math.random() * (i + 1)), 1)[0];
//     randomRoundArray.push(random);
// }

secondRoundValue.sort(function() {
    return .5 - Math.random();
  });

  secondRoundValueRandom = secondRoundValue;

// for (secondRoundValue;  i = secondRoundValue.length; i--) {
//     var random = secondRoundValue.splice(Math.floor(Math.random() * (i + 1)), 1)[0];
//     secondRoundValueRandom.push(random);
// }


randomRoundArray = roundArray;

let sixResetFlag = 0;
let isSixNeededRound = 0;

let isSeriesCountSixMax = 0; 

let timer = setInterval(() => {

     if(currentCount==48){
         clearTimeout(timer); 

        // console.log(numberRepeatCount);
        //  console.log(diceValues);
        // console.log(currentSum);

        callback({
            diceSum:currentSum,
            dices:diceValues,
            secondDice:secondDiceValue,
            thirdDice:thirdDiceValue,
            numbers:numberRepeatCount
        });
         return ;
     }   

   

 
    let randomRoundValue = randomRoundArray[currentCount];

    // let randomDiceValue = this.generateRandomNumber();

    let randomDiceValue =  finalArray[currentCount]



    if(isThirdChance){

        isThirdChance = false;


       

        let min=1; 
        let max=6;  
        let randomDiceValue =  Math.floor(Math.random() * (+max - +min)) + +min;


        if(thirdDiceValue.length == 0){
            thirdDiceValue.push(randomDiceValue);

        }else{

                let diceValue = 6 - thirdDiceValue[0];
            thirdDiceValue.push(diceValue);

        }

        return;

    }

    if(isSecondChance){

        let secondDice = secondRoundValueRandom[numberRepeatCount[5]-1];
        secondDiceValue.push(secondDice);
        if(secondDice == 6)
        isThirdChance = true;
        
        isSecondChance = false;
        return;

    }


    
    if(!diceValues[randomRoundValue-1]){



       
        
          
                if(randomDiceValue == 6){

                   
                   
                        isSeriesCountSixMax++;
                        currentSum +=randomDiceValue;
                        diceValues[randomRoundValue-1] = randomDiceValue;

                        numberRepeatCount[randomDiceValue-1]++;
                        currentCount++;
                        isSixNeededRound = 0;
                        isSecondChance = true;
                    
                }else{
                    isSeriesCountSixMax = 0;
                    currentSum +=randomDiceValue;
                    diceValues[randomRoundValue-1] = randomDiceValue;
                    diceValues[currentCount] = randomDiceValue;

                    numberRepeatCount[randomDiceValue-1]++;
                    currentCount++;
                    if(numberRepeatCount[5] <8)

                    isSixNeededRound++;
                }
              
            }
           
        
      

    
    






},1);

}


 generateRandomNumber(){
    let min=1; 
    let max=7;  
    return Math.floor(Math.random() * (+max - +min)) + +min;


}

}

module.exports = PlayerMoves;