
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/room', function(req, res){
  res.sendFile(__dirname + '/room.html');
});


var numUsers =0;

var roomData = new Map();
var clientRoom = new Map();


io.on('connection', (socket) => {

  console.log("new user connected");



  socket.on('test-unity-in', (item) => {
    console.log("unity test work",item);

      io.emit('test-unity-out',{...item,sessionId: socket.id});

  });


  var addedUser = false;



  // when the client emits 'add user', this listens and executes
  socket.on('join-room', (userData) => {

    console.log(userData);
    io.emit("current-player", userData);
    if (addedUser) return;

    // we store the username in the socket session for this client


    if(roomData.get(userData.room_name)!=null && roomData.get(userData.room_name)==2 ){

          socket.emit('room-join-failed', 'room already started');
          return;
    }

    socket.join(userData.room_name);

    socket.username = userData.username;
    ++numUsers;
    addedUser = true;


    clientRoom.set(socket.id, userData.room_name);
    if(roomData.get(userData.room_name)!=null){
      var userCount = roomData.get(userData.room_name);

      userCount += 1;


      roomData.set(userData.room_name,userCount);

      if(userCount == 2){

                  io.in(userData.room_name).emit('start-game',currentPlayerIndex);
                  io.in(userData.room_name).emit('restart-timer', currentTime);

                  startGamePlay(userData.room_name);

      }
    }else{
      roomData.set(userData.room_name, 1);

    }



    socket.emit('login', {
      roomName : userData.room_name,
      playerCount : roomData.get(userData.room_name)
    });
    // echo globally (all clients) that a person has connected
    // io.emit('room_joined', {
    //   username: socket.username,
    //   numUsers: numUsers
    // });


  });



  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    if (addedUser) {
      --numUsers;


      var roomName = clientRoom.get(socket.id);
      if(roomData.get(roomName)!=null){
        var userCount = roomData.get(roomName);

        userCount -= 1;


        roomData.set(roomName,userCount);
      }

      clientRoom.delete(socket.id);




      // echo globally that this client has left

      io.in(roomName).emit('user_left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });






    socket.on('dice-rolled', function(playType){



      io.in(clientRoom.get(socket.id)).emit('dice-value',playType.number);



          diceRolled(playType,clientRoom.get(socket.id) );
      });


});




const PORT = Number(process.env.PORT || 5000);

http.listen(PORT, function(){
  console.log('listening on *: '+PORT);
});



var count = 0;

var playerArray = ['a','b','c','d'];
var currentPlayerIndex = 0;
var playType = ['normal','six','kill','skip','home','timeout'];
var currentPlayType = 0;
var timer;
var gameTimeout;
var timerValue = 15000;
var currentTime = timerValue;






function countdownTimer(roomName){

setInterval(function () {

currentTime -=1;
}, 1000);
}
function startGamePlay(roomName){


 timer = generateTimer(roomName);
 countdownTimer(roomName);








}


function resetTimer(resetTimer){


  currentTime = timerValue;
  clearInterval(timer);
  timer = generateTimer();


    io.in(resetTimer).emit('restart-timer', currentTime);



}

function timerUp(roomName){
  console.log("Timer Up");
  currentPlayType = 5;
  switchPlayerIndex();
  // playTurn();
  skipTurn(roomName);
}


function skipTurn(roomName){

  io.in(roomName).emit('restart-timer', currentTime);
  io.in(roomName).emit('current-player', currentPlayerIndex);


}

function generateTimer(roomName){
  currentTime = timerValue;
   // gameTimeout = playTurn();


  return setInterval(function () {

    // console.log("Timer Called");

    clearInterval(gameTimeout);
    timerUp(roomName);

  }, timerValue);
}


function createInterval(timeInSecond, timeout) {

  clearInterval(timeout);

  var newTimeout = setTimeout(function () {

      // var min=1;
      // var max=5000;
      // var random = Math.random() * (+max - +min) + +min;
      //
      // random = Math.floor(random);
      // console.log("New Timeout at "+random+" seconds");

      createInterval(timerValue, newTimeout);


  }, timeInSecond);

}


function diceRolled(type,roomName){




      currentPlayType = type.type;
        console.log("Current Player : "+playerArray[currentPlayerIndex]);
        // setPlayType();
        // console.log("Play Type : "+playType[currentPlayType]);

        switchPlayerIndex();
        resetTimer(roomName);

        console.log("New Player : "+playerArray[currentPlayerIndex]);
        io.in(roomName).emit('current-player', currentPlayerIndex);



}


function playTurn(){



  io.emit('current-player', currentPlayerIndex);

  var min=1;
  var max=16000;
  var random = Math.random() * (+max - +min) + +min;

 random = Math.floor(random);




  return timeout  = setTimeout(function(){


    console.log("-------- New Round --------");
    console.log("---- Current Time : "+currentTime+" millisecond-----");


      console.log("Current Player : "+playerArray[currentPlayerIndex]);
      setPlayType();
      console.log("Play Type : "+playType[currentPlayType]);

      switchPlayerIndex();
      console.log("New Player : "+playerArray[currentPlayerIndex]);




      resetTimer();




  }, random);



}

function switchPlayerIndex(){

switch (currentPlayType) {
  case 0:
// normal
    currentPlayerIndex++;

    break;
  case 1:
  // six

    break;
  case 2:
  //kill

    break;
  case 3:
  // skip
  currentPlayerIndex++;

    break;
  case 4:
  //home

    break;
  case 5:
    //timeout
    currentPlayerIndex++;
    break;
  default:
  //normal
  currentPlayerIndex++;

}

if(currentPlayerIndex>3){
  currentPlayerIndex = 0;
}

}
function setPlayType(){

   var min=0;
   var max=5;
   var random = Math.random() * (+max - +min) + +min;

  currentPlayType  = Math.floor(random);




}
